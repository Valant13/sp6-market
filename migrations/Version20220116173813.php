<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Apartment\ApartmentType;
use App\Entity\Apartment\BuildingType;
use App\Entity\Apartment\Demand;
use App\Entity\Material\Material;
use App\Entity\Material\MaterialType;
use App\Entity\Period;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220116173813 extends AbstractMigration implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine.orm.entity_manager');

        $entityManager->persist($this->createDemandItem(ApartmentType::ONE_BEDROOM, BuildingType::HIGH_RISE));
        $entityManager->persist($this->createDemandItem(ApartmentType::TWO_BEDROOM, BuildingType::HIGH_RISE));
        $entityManager->persist($this->createDemandItem(ApartmentType::THREE_BEDROOM, BuildingType::HIGH_RISE));
        $entityManager->persist($this->createDemandItem(ApartmentType::ONE_BEDROOM, BuildingType::MID_RISE));
        $entityManager->persist($this->createDemandItem(ApartmentType::TWO_BEDROOM, BuildingType::MID_RISE));
        $entityManager->persist($this->createDemandItem(ApartmentType::THREE_BEDROOM, BuildingType::MID_RISE));
        $entityManager->persist($this->createDemandItem(ApartmentType::ONE_BEDROOM, BuildingType::LOW_RISE));
        $entityManager->persist($this->createDemandItem(ApartmentType::TWO_BEDROOM, BuildingType::LOW_RISE));
        $entityManager->persist($this->createDemandItem(ApartmentType::THREE_BEDROOM, BuildingType::LOW_RISE));

        $entityManager->persist($this->createMaterial(MaterialType::BRICKS));
        $entityManager->persist($this->createMaterial(MaterialType::STEEL));
        $entityManager->persist($this->createMaterial(MaterialType::CONCRETE));

        $entityManager->persist($this->createPeriod());

        $entityManager->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param string $apartmentType
     * @param string $buildingType
     * @return Demand
     */
    public function createDemandItem(string $apartmentType, string $buildingType): Demand
    {
        $demand = new Demand();

        $demand->setApartmentType($apartmentType);
        $demand->setBuildingType($buildingType);
        $demand->setApartmentQty(0);

        return $demand;
    }

    /**
     * @param string $materialType
     * @return Material
     */
    public function createMaterial(string $materialType): Material
    {
        $material = new Material();

        $material->setMaterialType($materialType);
        $material->setPrice(0);

        return $material;
    }

    /**
     * @return Period
     */
    public function createPeriod(): Period
    {
        $period = new Period();

        $period->setYear(0);

        return $period;
    }
}
