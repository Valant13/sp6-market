<?php

namespace App;

class Config
{
    const STARTING_YEAR = 1992;

    const MIN_DEMAND = 10;
    const MAX_DEMAND = 750;

    const MIN_MATERIAL_PRICE = 10;
    const MAX_MATERIAL_PRICE = 100;

    const MIN_LOT_PRICE = 1000;
    const MAX_LOT_PRICE = 10000;
    const MIN_LOT_AREA = 100;
    const MAX_LOT_AREA = 200;
    const MIN_LOTS_QTY = 10;
    const MAX_LOTS_QTY = 20;

    const CUSTOMER_FIRST_NAMES = [
        'Danil',
        'Artem',
        'Anna',
        'Vladislav',
        'Vitalij',
        'Maksim',
        'Galina',
        'Irina',
        'Oksana',
        'Evgenia',
        'Inna',
        'Evgenij',
        'Gennadij',
        'Sergij',
        'Taras',
        'Stanislav',
        'Ruslan'
    ];
    const CUSTOMER_LAST_NAMES = [
        'Brovarcuk',
        'Vasilcuk',
        'Melnicenko',
        'Vasilev',
        'Sereda',
        'Antonenko',
        'Kravcenko',
        'Shevcenko',
        'Petrenko',
        'Vasilenko',
        'Ponomarenko',
        'Mirosnicenko',
        'Tarasuk',
        'Sergienko',
        'Gnatuk',
        'Kramarcuk'
    ];
}
