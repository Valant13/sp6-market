<?php

namespace App\Controller;

use App\Entity\Apartment\Apartment;
use App\Entity\Apartment\ApartmentInvoice;
use App\Service\ApartmentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApartmentController extends AbstractController
{
    /**
     * @var ApartmentService
     */
    private $apartmentService;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param ApartmentService $apartmentService
     * @param RequestStack $requestStack
     */
    public function __construct(
        ApartmentService $apartmentService,
        RequestStack $requestStack
    ) {
        $this->apartmentService = $apartmentService;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @Route("/apartment/demand", methods="GET", name="apartment_demand")
     */
    public function demand(): Response
    {
        return $this->json($this->apartmentService->getCurrentDemand());
    }

    /**
     * @Route("/apartment/offer-apartments", methods="POST", name="apartment_offer_apartments")
     */
    public function offerApartments(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        /** @var Apartment[] $apartments */
        $apartments = [];
        foreach ($data as $dataItem) {
            $apartment = new Apartment();
            $apartment->setId($dataItem['id']);
            $apartment->setApartmentType($dataItem['apartment_type']);
            $apartment->setBuildingType($dataItem['building_type']);
            $apartment->setPrice($dataItem['price']);

            $apartments[] = $apartment;
        }

        $orders = $this->apartmentService->createOrdersForApartments($apartments);

        $result = [];
        foreach ($orders as $order) {
            $result[] = [
                'customer' => [
                    'first_name' => $order->getCustomer()->getFirstName(),
                    'last_name' => $order->getCustomer()->getLastName()
                ],
                'apartment_id' => $order->getApartmentId()
            ];
        }

        return $this->json($result);
    }

    /**
     * @Route("/apartment/apartment-invoices", methods="POST", name="apartment_apartment_invoices")
     */
    public function apartmentInvoices(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        foreach ($data as $dataItem) {
            $invoice = new ApartmentInvoice();
            $invoice->setApartmentType($dataItem['apartment_type']);
            $invoice->setBuildingType($dataItem['building_type']);

            $this->getDoctrine()->getManager()->persist($invoice);
        }

        $this->getDoctrine()->getManager()->flush();

        return new Response();
    }
}
