<?php

namespace App\Controller;

use App\Entity\Lot\LotOrder;
use App\Repository\Lot\LotRepository;
use App\Service\LotService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LotController extends AbstractController
{
    /**
     * @var LotService
     */
    private $lotService;

    /**
     * @var LotRepository
     */
    private $lotRepository;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param LotService $lotService
     * @param LotRepository $lotRepository
     * @param RequestStack $requestStack
     */
    public function __construct(
        LotService $lotService,
        LotRepository $lotRepository,
        RequestStack $requestStack
    ) {
        $this->lotService = $lotService;
        $this->lotRepository = $lotRepository;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @Route("/lot/lots", methods="GET", name="lot_lots")
     */
    public function lots(): Response
    {
        $lots = $this->lotRepository->findAll();

        $result = [];
        foreach ($lots as $lot) {
            $result[] = [
                'id' => $lot->getId(),
                'area' => $lot->getArea(),
                'price' => $lot->getPrice()
            ];
        }

        return $this->json($result);
    }

    /**
     * @Route("/lot/place-order", methods="POST", name="lot_place_order")
     */
    public function placeOrder(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $order = new LotOrder();
        $order->setLotId($data['lot_id']);

        $invoice = $this->lotService->placeOrder($order);

        $result = [
            'lot_id' => $invoice->getLotId(),
            'lot_price' => $invoice->getLotPrice(),
            'total' => $invoice->getTotal()
        ];

        return $this->json($result);
    }
}
