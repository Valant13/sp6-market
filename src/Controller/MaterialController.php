<?php

namespace App\Controller;

use App\Entity\Material\MaterialOrder;
use App\Repository\Material\MaterialRepository;
use App\Service\MaterialService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaterialController extends AbstractController
{
    /**
     * @var MaterialService
     */
    private $materialService;

    /**
     * @var MaterialRepository
     */
    private $materialRepository;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param MaterialService $materialService
     * @param MaterialRepository $materialRepository
     * @param RequestStack $requestStack
     */
    public function __construct(
        MaterialService $materialService,
        MaterialRepository $materialRepository,
        RequestStack $requestStack
    ) {
        $this->materialService = $materialService;
        $this->materialRepository = $materialRepository;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @Route("/material/materials", methods="GET", name="material_materials")
     */
    public function materials(): Response
    {
        $materials = $this->materialRepository->findAll();

        $result = [];
        foreach ($materials as $material) {
            $result[] = [
                'material_type' => $material->getMaterialType(),
                'price' => $material->getPrice()
            ];
        }

        return $this->json($result);
    }

    /**
     * @Route("/material/place-order", methods="POST", name="material_place_order")
     */
    public function placeOrder(): Response
    {
        $data = json_decode($this->request->getContent(), true);

        $order = new MaterialOrder();
        $order->setMaterialType($data['material_type']);
        $order->setMaterialQty($data['material_qty']);

        $invoice = $this->materialService->placeOrder($order);

        $result = [
            'material_type' => $invoice->getMaterialType(),
            'material_qty' => $invoice->getMaterialQty(),
            'material_price' => $invoice->getMaterialPrice(),
            'total' => $invoice->getTotal()
        ];

        return $this->json($result);
    }
}
