<?php

namespace App\Controller;

use App\Service\PeriodService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PeriodController extends AbstractController
{
    /**
     * @var PeriodService
     */
    private $periodService;

    /**
     * @param PeriodService $periodService
     */
    public function __construct(PeriodService $periodService)
    {
        $this->periodService = $periodService;
    }

    /**
     * @Route("/period/set-next-year", methods="GET", name="period_set_next_year")
     */
    public function setNextYear(): Response
    {
        $this->periodService->setNextYear();
        $currentYear = $this->periodService->getCurrentYear();

        return new Response($currentYear);
    }

    /**
     * @Route("/period/reset-year", methods="GET", name="period_reset_year")
     */
    public function resetYear(): Response
    {
        $this->periodService->resetYear();
        $currentYear = $this->periodService->getCurrentYear();

        return new Response($currentYear);
    }

    /**
     * @Route("/period/year", methods="GET", name="period_year")
     */
    public function year(): Response
    {
        $currentYear = $this->periodService->getCurrentYear();

        return new Response($currentYear);
    }
}
