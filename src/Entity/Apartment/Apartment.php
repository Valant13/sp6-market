<?php

namespace App\Entity\Apartment;

class Apartment
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $apartmentType;

    /**
     * @var string
     */
    private $buildingType;

    /**
     * @var int
     */
    private $price;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getApartmentType(): string
    {
        return $this->apartmentType;
    }

    /**
     * @param string $apartmentType
     */
    public function setApartmentType(string $apartmentType): void
    {
        $this->apartmentType = $apartmentType;
    }

    /**
     * @return string
     */
    public function getBuildingType(): string
    {
        return $this->buildingType;
    }

    /**
     * @param string $buildingType
     */
    public function setBuildingType(string $buildingType): void
    {
        $this->buildingType = $buildingType;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }
}
