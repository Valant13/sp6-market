<?php

namespace App\Entity\Apartment;

use App\Repository\ApartmentInvoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApartmentInvoiceRepository::class)
 */
class ApartmentInvoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apartmentType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $buildingType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApartmentType(): ?string
    {
        return $this->apartmentType;
    }

    public function setApartmentType(string $apartmentType): self
    {
        $this->apartmentType = $apartmentType;

        return $this;
    }

    public function getBuildingType(): ?string
    {
        return $this->buildingType;
    }

    public function setBuildingType(string $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }
}
