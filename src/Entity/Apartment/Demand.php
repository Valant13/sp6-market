<?php

namespace App\Entity\Apartment;

use App\Repository\DemandRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DemandRepository::class)
 */
class Demand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apartmentType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $buildingType;

    /**
     * @ORM\Column(type="integer")
     */
    private $apartmentQty;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApartmentType(): ?string
    {
        return $this->apartmentType;
    }

    public function setApartmentType(string $apartmentType): self
    {
        $this->apartmentType = $apartmentType;

        return $this;
    }

    public function getBuildingType(): ?string
    {
        return $this->buildingType;
    }

    public function setBuildingType(string $buildingType): self
    {
        $this->buildingType = $buildingType;

        return $this;
    }

    public function getApartmentQty(): ?int
    {
        return $this->apartmentQty;
    }

    public function setApartmentQty(int $apartmentQty): self
    {
        $this->apartmentQty = $apartmentQty;

        return $this;
    }
}
