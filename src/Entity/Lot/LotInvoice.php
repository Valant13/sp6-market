<?php

namespace App\Entity\Lot;

class LotInvoice
{
    /**
     * @var int
     */
    private $lotId;

    /**
     * @var int
     */
    private $lotPrice;

    /**
     * @var int
     */
    private $total;

    /**
     * @return int
     */
    public function getLotId(): int
    {
        return $this->lotId;
    }

    /**
     * @param int $lotId
     */
    public function setLotId(int $lotId): void
    {
        $this->lotId = $lotId;
    }

    /**
     * @return int
     */
    public function getLotPrice(): int
    {
        return $this->lotPrice;
    }

    /**
     * @param int $lotPrice
     */
    public function setLotPrice(int $lotPrice): void
    {
        $this->lotPrice = $lotPrice;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }
}
