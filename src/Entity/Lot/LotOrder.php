<?php

namespace App\Entity\Lot;

class LotOrder
{
    /**
     * @var int
     */
    private $lotId;

    /**
     * @return int
     */
    public function getLotId(): int
    {
        return $this->lotId;
    }

    /**
     * @param int $lotId
     */
    public function setLotId(int $lotId): void
    {
        $this->lotId = $lotId;
    }
}
