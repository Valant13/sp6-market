<?php

namespace App\Entity\Material;

class MaterialInvoice
{
    /**
     * @var string
     */
    private $materialType;

    /**
     * @var int
     */
    private $materialPrice;

    /**
     * @var int
     */
    private $materialQty;

    /**
     * @var int
     */
    private $total;

    /**
     * @return string
     */
    public function getMaterialType(): string
    {
        return $this->materialType;
    }

    /**
     * @param string $materialType
     */
    public function setMaterialType(string $materialType): void
    {
        $this->materialType = $materialType;
    }

    /**
     * @return int
     */
    public function getMaterialPrice(): int
    {
        return $this->materialPrice;
    }

    /**
     * @param int $materialPrice
     */
    public function setMaterialPrice(int $materialPrice): void
    {
        $this->materialPrice = $materialPrice;
    }

    /**
     * @return int
     */
    public function getMaterialQty(): int
    {
        return $this->materialQty;
    }

    /**
     * @param int $materialQty
     */
    public function setMaterialQty(int $materialQty): void
    {
        $this->materialQty = $materialQty;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }
}
