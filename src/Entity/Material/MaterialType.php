<?php

namespace App\Entity\Material;

class MaterialType
{
    const STEEL = 'steel';
    const CONCRETE = 'concrete';
    const BRICKS = 'bricks';
}
