<?php

namespace App\Repository\Apartment;

use App\Entity\Apartment\ApartmentInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApartmentInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApartmentInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApartmentInvoice[]    findAll()
 * @method ApartmentInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApartmentInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApartmentInvoice::class);
    }

    /**
     * @param string $apartmentType
     * @param string $buildingType
     * @return int
     */
    public function countByApartment(string $apartmentType, string $buildingType): int
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.apartmentType = :apartmentType')
            ->andWhere('a.buildingType = :buildingType')
            ->setParameter('apartmentType', $apartmentType)
            ->setParameter('buildingType', $buildingType)
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
