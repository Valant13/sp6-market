<?php

namespace App\Repository\Material;

use App\Entity\Material\Material;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Material|null find($id, $lockMode = null, $lockVersion = null)
 * @method Material|null findOneBy(array $criteria, array $orderBy = null)
 * @method Material[]    findAll()
 * @method Material[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Material::class);
    }

    /**
     * @param string $materialType
     * @return Material|null
     */
    public function findOneByMaterialType(string $materialType): ?Material
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.materialType = :materialType')
            ->setParameter('materialType', $materialType)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
