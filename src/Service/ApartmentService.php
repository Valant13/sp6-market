<?php

namespace App\Service;

use App\Config;
use App\Entity\Apartment\Apartment;
use App\Entity\Apartment\ApartmentOrder;
use App\Entity\Apartment\Customer;
use App\Entity\Apartment\Demand;
use App\Repository\Apartment\ApartmentInvoiceRepository;
use App\Repository\Apartment\DemandRepository;
use Doctrine\ORM\EntityManagerInterface;

class ApartmentService
{
    /**
     * @var DemandRepository
     */
    private $demandRepository;

    /**
     * @var ApartmentInvoiceRepository
     */
    private $apartmentInvoiceRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param DemandRepository $demandRepository
     * @param ApartmentInvoiceRepository $apartmentInvoiceRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        DemandRepository $demandRepository,
        ApartmentInvoiceRepository $apartmentInvoiceRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->demandRepository = $demandRepository;
        $this->apartmentInvoiceRepository = $apartmentInvoiceRepository;
        $this->entityManager = $entityManager;
    }

    /**
     *
     */
    public function setRandomDemand(): void
    {
        $demandItems = $this->demandRepository->findAll();

        foreach ($demandItems as $demandItem) {
            $demandItem->setApartmentQty(rand(Config::MIN_DEMAND, Config::MAX_DEMAND));
        }

        $this->entityManager->flush();
    }

    /**
     * Get current demand in form of map
     *
     * $map[$apartmentType][$buildingType] = $apartmentQty
     *
     * @return array
     */
    public function getCurrentDemand(): array
    {
        $map = [];

        $demandItems = $this->demandRepository->findAll();
        foreach ($demandItems as $demandItem) {
            $apartmentType = $demandItem->getApartmentType();
            $buildingType = $demandItem->getBuildingType();

            if (!array_key_exists($apartmentType, $map)) {
                $map[$apartmentType] = [];
            }

            $soldApartmentsQty = $this->apartmentInvoiceRepository->countByApartment($apartmentType, $buildingType);

            $map[$apartmentType][$buildingType] = $demandItem->getApartmentQty() - $soldApartmentsQty;
        }

        return $map;
    }

    /**
     * @param Apartment[] $apartments
     * @return ApartmentOrder[]
     */
    public function createOrdersForApartments(array $apartments): array
    {
        $orders = [];

        $demand = $this->getCurrentDemand();

        // TODO: sort apartments by price first
        foreach ($apartments as $apartment) {
            $apartmentType = $apartment->getApartmentType();
            $buildingType = $apartment->getBuildingType();

            if ($demand[$apartmentType][$buildingType] > 0) {
                $orders[] = $this->createOrderForApartment($apartment);

                $demand[$apartmentType][$buildingType]--;
            }
        }

        return $orders;
    }

    /**
     * @param Apartment $apartment
     * @return ApartmentOrder
     */
    private function createOrderForApartment(Apartment $apartment): ApartmentOrder
    {
        $customer = new Customer();
        $customer->setFirstName(Config::CUSTOMER_FIRST_NAMES[array_rand(Config::CUSTOMER_FIRST_NAMES)]);
        $customer->setLastName(Config::CUSTOMER_LAST_NAMES[array_rand(Config::CUSTOMER_LAST_NAMES)]);

        $order = new ApartmentOrder();
        $order->setCustomer($customer);
        $order->setApartmentId($apartment->getId());

        return $order;
    }
}
