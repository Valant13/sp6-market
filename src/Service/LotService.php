<?php

namespace App\Service;

use App\Config;
use App\Entity\Lot\Lot;
use App\Entity\Lot\LotInvoice;
use App\Entity\Lot\LotOrder;
use App\Repository\Lot\LotRepository;
use Doctrine\ORM\EntityManagerInterface;

class LotService
{
    /**
     * @var LotRepository
     */
    private $lotRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param LotRepository $lotRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        LotRepository $lotRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->lotRepository = $lotRepository;
        $this->entityManager = $entityManager;
    }

    /**
     *
     */
    public function recreateLots(): void
    {
        $lots = $this->lotRepository->findAll();
        foreach ($lots as $lot) {
            $this->entityManager->remove($lot);
        }
        $this->entityManager->flush();

        $lotsQty = rand(Config::MIN_LOTS_QTY, Config::MAX_LOTS_QTY);
        for ($i = 0; $i < $lotsQty; $i++) {
            $lot = new Lot();
            $lot->setArea(rand(Config::MIN_LOT_AREA, Config::MAX_LOT_AREA));
            $lot->setPrice(rand(Config::MIN_LOT_PRICE, Config::MAX_LOT_PRICE));

            $this->entityManager->persist($lot);
        }
        $this->entityManager->flush();
    }

    /**
     * @param LotOrder $lotOrder
     * @return LotInvoice
     * @throws \Exception
     */
    public function placeOrder(LotOrder $lotOrder): LotInvoice
    {
        $lotId = $lotOrder->getLotId();

        $lot = $this->lotRepository->find($lotId);
        if ($lot === null) {
            throw new \Exception("Lot with ID $lotId does not exist");
        }

        $invoice = new LotInvoice();
        $invoice->setLotId($lot->getId());
        $invoice->setLotPrice($lot->getPrice());
        $invoice->setTotal($lot->getPrice());

        return $invoice;
    }
}
