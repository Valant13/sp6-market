<?php

namespace App\Service;

use App\Config;
use App\Entity\Material\MaterialInvoice;
use App\Entity\Material\MaterialOrder;
use App\Repository\Material\MaterialRepository;
use Doctrine\ORM\EntityManagerInterface;

class MaterialService
{
    /**
     * @var MaterialRepository
     */
    private $materialRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param MaterialRepository $materialRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        MaterialRepository $materialRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->materialRepository = $materialRepository;
        $this->entityManager = $entityManager;
    }

    /**
     *
     */
    public function setRandomMaterialPrices(): void
    {
        $materials = $this->materialRepository->findAll();

        foreach ($materials as $material) {
            $material->setPrice(rand(Config::MIN_MATERIAL_PRICE, Config::MAX_MATERIAL_PRICE));
        }

        $this->entityManager->flush();
    }

    /**
     * @param MaterialOrder $materialOrder
     * @return MaterialInvoice
     * @throws \Exception
     */
    public function placeOrder(MaterialOrder $materialOrder): MaterialInvoice
    {
        $materialType = $materialOrder->getMaterialType();

        $material = $this->materialRepository->findOneByMaterialType($materialType);
        if ($material === null) {
            throw new \Exception("Material of type $materialType does not exist");
        }

        $invoice = new MaterialInvoice();
        $invoice->setMaterialType($material->getMaterialType());
        $invoice->setMaterialQty($materialOrder->getMaterialQty());
        $invoice->setMaterialPrice($material->getPrice());
        $invoice->setTotal($material->getPrice() * $materialOrder->getMaterialQty());

        return $invoice;
    }
}
