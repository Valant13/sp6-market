<?php

namespace App\Service\PeriodHandler;

use App\Repository\Apartment\ApartmentInvoiceRepository;
use App\Service\ApartmentService;
use App\Service\PeriodHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class ApartmentHandler implements PeriodHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ApartmentInvoiceRepository
     */
    private $apartmentInvoiceRepository;

    /**
     * @var ApartmentService
     */
    private $apartmentService;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ApartmentInvoiceRepository $apartmentInvoiceRepository
     * @param ApartmentService $apartmentService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ApartmentInvoiceRepository $apartmentInvoiceRepository,
        ApartmentService $apartmentService
    ) {
        $this->entityManager = $entityManager;
        $this->apartmentInvoiceRepository = $apartmentInvoiceRepository;
        $this->apartmentService = $apartmentService;
    }

    public function reset(): void
    {
        $this->apartmentService->setRandomDemand();

        // TODO: find better way to delete all entities
        $invoices = $this->apartmentInvoiceRepository->findAll();
        foreach ($invoices as $invoice) {
            $this->entityManager->remove($invoice);
        }

        $this->entityManager->flush();
    }
}
