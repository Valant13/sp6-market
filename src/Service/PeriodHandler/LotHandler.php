<?php

namespace App\Service\PeriodHandler;

use App\Service\LotService;
use App\Service\PeriodHandlerInterface;

class LotHandler implements PeriodHandlerInterface
{
    /**
     * @var LotService
     */
    private $lotService;

    /**
     * @param LotService $lotService
     */
    public function __construct(LotService $lotService)
    {
        $this->lotService = $lotService;
    }

    public function reset(): void
    {
        $this->lotService->recreateLots();
    }
}
