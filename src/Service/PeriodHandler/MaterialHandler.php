<?php

namespace App\Service\PeriodHandler;

use App\Service\MaterialService;
use App\Service\PeriodHandlerInterface;

class MaterialHandler implements PeriodHandlerInterface
{
    /**
     * @var MaterialService
     */
    private $materialService;

    /**
     * @param MaterialService $materialService
     */
    public function __construct(MaterialService $materialService)
    {
        $this->materialService = $materialService;
    }

    public function reset(): void
    {
        $this->materialService->setRandomMaterialPrices();
    }
}
