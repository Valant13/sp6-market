<?php

namespace App\Service;

use App\Config;
use App\Repository\PeriodRepository;
use Doctrine\ORM\EntityManagerInterface;

class PeriodService
{
    /**
     * @var PeriodRepository
     */
    private $periodRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PeriodHandlerInterface[]
     */
    private $handlers;

    /**
     * @param PeriodRepository $periodRepository
     * @param EntityManagerInterface $entityManager
     * @param PeriodHandlerInterface[] $handlers
     */
    public function __construct(
        PeriodRepository $periodRepository,
        EntityManagerInterface $entityManager,
        array $handlers
    ) {
        $this->periodRepository = $periodRepository;
        $this->entityManager = $entityManager;
        $this->handlers = $handlers;
    }

    /**
     *
     */
    public function setNextYear(): void
    {
        $this->setYear($this->getCurrentYear() + 1);
        $this->resetAppState();
    }

    /**
     *
     */
    public function resetYear(): void
    {
        $this->setYear(Config::STARTING_YEAR);
        $this->resetAppState();
    }

    /**
     * @return int
     */
    public function getCurrentYear(): int
    {
        return $this->periodRepository->findOne()->getYear();
    }

    /**
     * @param int $year
     */
    private function setYear(int $year): void
    {
        $period = $this->periodRepository->findOne();
        $period->setYear($year);
        $this->entityManager->flush();
    }

    /**
     *
     */
    private function resetAppState(): void
    {
        foreach ($this->handlers as $handler) {
            $handler->reset();
        }
    }
}
